FROM quay.io/prometheus/busybox:latest
LABEL maintainer="Ihor Savchenko <ihor.i.savchenko@gmail.com>"

WORKDIR /src

ARG TARGETOS
ARG TARGETARCH
ARG RPI_EXPORTER_VERSION=0.7.0
#RUN wget https://github.com/lukasmalkmus/rpi_exporter/releases/download/v${RPI_EXPORTER_VERSION}/rpi_exporter-${RPI_EXPORTER_VERSION}.${TARGETOS}-${TARGETARCH}.tar.gz -O - \
RUN wget -c https://github.com/lukasmalkmus/rpi_exporter/releases/download/v0.7.0/rpi_exporter-0.7.0.linux-arm64.tar.gz -O - \
    | tar -xz rpi_exporter-${RPI_EXPORTER_VERSION}.${TARGETOS}-${TARGETARCH}.tar.gz \
    && mv ./rpi_exporter-${RPI_EXPORTER_VERSION}.${TARGETOS}-${TARGETARCH}/rpi_exporter /bin/rpi_exporter

ENTRYPOINT ["rpi_exporter"]
EXPOSE     9243
